package org.mineacademy.orion.util;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.orion.tool.KittyArrow;

public class OrionUtil {

	// Check if player has kitty arrow, if not tells him an error message and cancels any cancellable event
	// given to this method as well as takes 1 piece of the arrow item if player is in survival
	public static boolean checkKittyArrow(final Player player, final Cancellable event) {

		// Find the first occurence of an item that is similar to Kitty Arrorw
		final ItemStack arrow = PlayerUtil.getFirstItem(player, KittyArrow.getInstance().getItem());

		if (arrow == null) {
			Common.tell(player, "&cYou lack the Kitty Arrow required to shoot from this tool!");

			event.setCancelled(true);
			return false;
		}

		if (player.getGameMode() == GameMode.SURVIVAL)
			// Takes 1 piece of the Kitty Arrow
			PlayerUtil.takeOnePiece(player, arrow);

		return true;
	}
}
