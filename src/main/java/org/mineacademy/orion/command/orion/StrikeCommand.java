package org.mineacademy.orion.command.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class StrikeCommand extends TargettedCommand {

	protected StrikeCommand(final SimpleCommandGroup parent) {
		super(parent, "strike|s");

		setDescription("Strike lightning at the target.");
	}

	@Override
	protected void onCommandFor(final Player target) {
		target.getWorld().strikeLightning(target.getLocation());
		tell("&bStroke lightning at " + target.getName());
	}
}
