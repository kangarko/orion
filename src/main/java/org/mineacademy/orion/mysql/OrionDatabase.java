package org.mineacademy.orion.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.database.SimpleFlatDatabase;
import org.mineacademy.fo.debug.Debugger;
import org.mineacademy.orion.PlayerCache;
import org.mineacademy.orion.vote.PlayerVote;

import lombok.Getter;
import lombok.NonNull;

/**
 * Handles MySQL database for player data, votes and pending votes.
 */
public final class OrionDatabase extends SimpleFlatDatabase<PlayerCache> {

	/**
	 * The instance for singleton access
	 */
	@Getter
	private final static OrionDatabase instance = new OrionDatabase();

	/**
	 * Create a new database (automatically in instance)
	 * and adds {votes} variable for the Orion_Votes table name as well
	 * as {pending_votes} variable for the Orion_Pending_Votes table
	 */
	private OrionDatabase() {
		addVariable("table", "Orion");
		addVariable("votes", "Orion_Votes");
		addVariable("pending_votes", "Orion_Pending_Votes");
	}

	/**
	 * Create two tables upon connecting - the table for completed votes
	 * as well as a table for storing pending votes
	 *
	 * The player data table is created beforehand in {@link #onConnected()} already
	 * in {@link SimpleFlatDatabase}
	 */
	@Override
	protected void onConnectFinish() {
		update("CREATE TABLE IF NOT EXISTS {votes}(Name text, Service text, Date bigint)");
		update("CREATE TABLE IF NOT EXISTS {pending_votes}(Name text, Pending int)");
	}

	// --------------------------------------------------------------------------------------------------------------
	// Manage player data
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Load the player data to the {@link PlayerCache}
	 *
	 * @param map
	 * @param data
	 */
	@Override
	protected void onLoad(final SerializedMap map, final PlayerCache data) {
		final ChatColor color = map.get("Color", ChatColor.class);

		if (color != null)
			data.setColor(color);

		// We must make the Bukkit API methods run on the main thread by synchronizing them
		/*Common.runLater(() -> {
			Bukkit.getWorld("world").getBlockAt(0,0,0).setType(Material.FLOWER_POT);
		});*/
	}

	/**
	 * Save the player data for the given cache
	 *
	 * @param data
	 */
	@Override
	protected SerializedMap onSave(final PlayerCache data) {
		final SerializedMap map = new SerializedMap();

		if (data.getColor() != null)
			map.put("Color", data.getColor());

		return map;
	}

	/**
	 * Return the amount of days after which we remove the player data (NOT vote data, just player data)
	 *
	 * @return
	 */
	@Override
	protected int getExpirationDays() {
		return 90; // 90 is the default value
	}

	// --------------------------------------------------------------------------------------------------------------
	// Pending votes management
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Saves a new pending vote into the database.
	 *
	 * If the player already has a pending vote no action is taken,
	 * otherwise we set his value of the row to "1"
	 *
	 * @param vote
	 */
	public void addPendingVote(final PlayerVote vote) {
		checkLoadedAndAsync();

		if (!hasPendingVote(vote.getPlayerName()))
			update("INSERT INTO {pending_votes}(Name, Pending) VALUES ('" + vote.getPlayerName() + "', '" + 1 + "');");
	}

	/**
	 * Remove all pending votes for the player
	 *
	 * @param player
	 */
	public void removePendingVotes(final Player player) {
		checkLoadedAndAsync();

		update("DELETE FROM {pending_votes} WHERE Name='" + player.getName() + "'");
	}

	/**
	 * Returns true if the player has a vote pending, any amount of pending votes
	 *
	 * @param playerName
	 * @return
	 */
	public boolean hasPendingVote(@NonNull final String playerName) {
		checkLoadedAndAsync();

		final ResultSet resultSet = query("SELECT * FROM {pending_votes} WHERE Name= '" + playerName + "'");

		try {
			if (resultSet == null)
				return false;

			if (resultSet.next())
				return resultSet.getInt("Pending") != 0;

		} catch (final SQLException ex) {
			Common.error(ex,
					"Failed to check pending votes from MySQL!",
					"Player: " + playerName,
					"Error: %error");
		}

		return false;
	}

	// --------------------------------------------------------------------------------------------------------------
	// Saved votes management
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Loads all saved votes for the player
	 *
	 * @param player
	 * @return
	 */
	public List<PlayerVote> loadVotes(final Player player) {
		checkLoadedAndAsync();

		try {
			final ResultSet resultSet = query("SELECT * FROM {votes} WHERE Name='" + player.getName() + "'");

			final List<PlayerVote> votes = new ArrayList<>();

			while (resultSet.next()) {
				final String service = resultSet.getString("Service");
				final long date = resultSet.getLong("Date");

				votes.add(new PlayerVote(player.getName(), service, date));
			}

			Debugger.debug("mysql", "Player " + player.getName() + " has voted " + votes.size() + " times");

			// Close connection at the end
			resultSet.close();

			return votes;

		} catch (final Throwable t) {
			Common.error(t,
					"Failed to load data from MySQL!",
					"Player: " + player.getName(),
					"Error: %error");

			return new ArrayList<>();
		}
	}

	/**
	 * Saves a vote for the player, this vote may or may not be pending.
	 *
	 * Pending votes are stored here as well as the separate PendingVotes table.
	 *
	 * @param vote
	 */
	public void saveVote(final PlayerVote vote) {
		checkLoadedAndAsync();

		try {
			final String name = vote.getPlayerName();
			final String service = vote.getServiceName();
			final long timestamp = vote.getTimestamp();

			update("INSERT INTO {votes}(Name, Service, Date) VALUES ('" + name + "', '" + service + "', '" + timestamp + "');");

		} catch (final Throwable ex) {
			Common.error(ex,
					"Failed to save data to MySQL!",
					"Player: " + vote.getPlayerName(),
					"Error: %error");

		}
	}

	/**
	 * Throws an error if the database is not loaded yet
	 * and if we are calling the method asynchronously
	 */
	private void checkLoadedAndAsync() {
		Valid.checkBoolean(isLoaded(), "Database not loaded, have you called connect() methods?");
		Valid.checkAsync("Database call must happen async!");
	}
}
