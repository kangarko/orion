package org.mineacademy.orion;

import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.remain.Remain;

public class BroadcasterTask extends BukkitRunnable {

	private final List<String> messages = Arrays.asList(
			"Check out our website https://mineacademy.org.",
			"You can support our server and purchase ranks at https://mineacademy.org/vip.",
			"Use the /orion command to run some fancy features of our custom plugin Orion.");

	@Override
	public void run() {
		final String prefix = "&8[&4Tip&8] &7";
		final String message = RandomUtil.nextItem(messages);

		for (final Player player : Remain.getOnlinePlayers())
			Common.tellNoPrefix(player, prefix + message);
	}
}
