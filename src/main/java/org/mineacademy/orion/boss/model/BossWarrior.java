package org.mineacademy.orion.boss.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompAttribute;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompItemFlag;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.orion.boss.skill.BossSkillBlockAttack;
import org.mineacademy.orion.boss.skill.BossSkillLightning;
import org.mineacademy.orion.boss.skill.BossSkillThrow;

public class BossWarrior extends Boss {

	protected BossWarrior() {
		super("Skeleton Warrior", EntityType.SKELETON);

		setCustomName("&4Skeleton Warrior");
		setHealth(5.0);

		setEquipment(
				buildWarriorArmor(CompMaterial.LEATHER_HELMET,
						"&cWarrior Helmet",
						"",
						"The legendary warrior",
						"armor with enchants."),

				buildWarriorArmor(CompMaterial.LEATHER_CHESTPLATE,
						"&cWarrior Chestplate",
						"",
						"The legendary warrior",
						"armor with enchants."),

				buildWarriorArmor(CompMaterial.LEATHER_LEGGINGS,
						"&cWarrior Leggings",
						"",
						"The legendary warrior",
						"armor with enchants."),

				buildWarriorArmor(CompMaterial.LEATHER_BOOTS,
						"&cWarrior Boots",
						"",
						"The legendary warrior",
						"armor with enchants."));

		setDroppedExp(500);
		addAttribute(CompAttribute.GENERIC_MOVEMENT_SPEED, 0.5);
		addPotionEffect(PotionEffectType.REGENERATION);
		setPassenger(EntityType.CAVE_SPIDER);

		addSkill(new BossSkillBlockAttack());
		addSkill(new BossSkillLightning());
		addSkill(new BossSkillThrow());
	}

	private final Map<UUID /*player*/, Double /*damage*/> damageMap = new HashMap<>();

	@Override
	public void onDeath(Player killer, LivingEntity bossEntity, EntityDeathEvent event) {

		// Copy the entry set as list so we can sort it
		final List<Entry<UUID, Double>> byMostDamage = new ArrayList<>(damageMap.entrySet());

		// Now we can clear the map
		damageMap.clear();

		// Sort the list, compare by values which is the damage (stored as a double)
		Collections.sort(byMostDamage, (first, second) -> Double.compare(first.getValue(), second.getValue())); // TODO check if you may want to reverse the first/second variables

		// Reward the first 3 players
		for (int order = 0; order < 2; order++)
			rewardPlayer(byMostDamage, order);
	}

	private void rewardPlayer(List<Entry<UUID, Double>> byMostDamage, int order) {
		if (byMostDamage.size() > order) {
			final Entry<UUID, Double> entry = byMostDamage.get(order);
			final Player player = Remain.getPlayerByUUID(entry.getKey());

			// Check if the such player exists and is online
			if (player != null && player.isOnline())
				onReward(player, entry.getValue(), order + 1 /*human readable order*/);
		}
	}

	// Reward the player for the given order
	private void onReward(Player player, Double damage, int order) {
		Common.tell(player, "You are the number " + order + " who damaged the boss dealing " + damage + " and you were now rewarded!");
	}

	@Override
	public void onAttack(LivingEntity bossAttacker, LivingEntity victim, EntityDamageByEntityEvent event) {

		if (bossAttacker instanceof Player)
			damageMap.put(bossAttacker.getUniqueId(), Remain.getFinalDamage(event));
	}

	private ItemCreator buildWarriorArmor(final CompMaterial material, final String title, final String... lore) {
		return ItemCreator.of(material, title, lore)
				.enchant(Enchantment.PROTECTION_ENVIRONMENTAL)
				.color(CompColor.RED)
				.flags(CompItemFlag.HIDE_ATTRIBUTES);
	}
}
