package org.mineacademy.orion.rank.model;

import org.bukkit.ChatColor;

public class RankHero extends Rank {

	protected RankHero() {
		super("Hero", ChatColor.DARK_AQUA);
	}

	@Override
	public Rank getNext() {
		return MASTER;
	}
}
