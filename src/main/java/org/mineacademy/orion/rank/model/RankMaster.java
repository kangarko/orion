package org.mineacademy.orion.rank.model;

import org.bukkit.ChatColor;

public class RankMaster extends Rank {

	protected RankMaster() {
		super("Master", ChatColor.RED);
	}
}
