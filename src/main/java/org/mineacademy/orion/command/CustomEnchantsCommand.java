package org.mineacademy.orion.command;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.orion.enchant.BlackNovaEnchant;
import org.mineacademy.orion.enchant.HideEnchant;

public class CustomEnchantsCommand extends SimpleCommand {

	public CustomEnchantsCommand() {
		super("customenchants|ce");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();

		// Add multiple items, comma separated
		player.getInventory().addItem(
				ItemCreator.of(CompMaterial.IRON_SWORD,
						"&bBlack Sword")
						.enchant(BlackNovaEnchant.getInstance())
						.make(),

				ItemCreator.of(CompMaterial.BOW)
						.enchant(HideEnchant.getInstance(), 2)
						.make());

		tell("&6You were given items with custom enchantments.");
	}
}
