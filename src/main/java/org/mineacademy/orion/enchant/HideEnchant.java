package org.mineacademy.orion.enchant;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.model.SimpleEnchantment;
import org.mineacademy.fo.remain.CompSound;

import lombok.Getter;

public final class HideEnchant extends SimpleEnchantment {

	@Getter
	private static final Enchantment instance = new HideEnchant();

	private HideEnchant() {
		super("Hide", 5);
	}

	// Called automatically only when the shooter has this particular enchant on his hand item
	@Override
	protected void onShoot(final int level, final LivingEntity shooter, final ProjectileLaunchEvent event) {
		if (!(shooter instanceof Player))
			return;

		final Player player = (Player) shooter;
		CompSound.ANVIL_LAND.play(player);
	}

	// Called automatically only when the shooter had this particular enchant on his hand item
	@Override
	protected void onHit(final int level, final LivingEntity shooter, final ProjectileHitEvent event) {
		if (!(event.getHitEntity() instanceof LivingEntity))
			return;

		final LivingEntity hitEntity = (LivingEntity) event.getHitEntity();

		hitEntity.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (level * 3) * 20, 0));
	}
}
