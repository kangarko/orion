package org.mineacademy.orion;

import org.bukkit.Material;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.model.Variables;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.orion.boss.BossListener;
import org.mineacademy.orion.boss.BossTimedTask;
import org.mineacademy.orion.classes.ClassListener;
import org.mineacademy.orion.classes.model.ClassBase;
import org.mineacademy.orion.command.BoardingCommand;
import org.mineacademy.orion.command.BossCommand;
import org.mineacademy.orion.command.ClassCommand;
import org.mineacademy.orion.command.CooldownCommand;
import org.mineacademy.orion.command.CustomEnchantsCommand;
import org.mineacademy.orion.command.FireworkCommand;
import org.mineacademy.orion.command.NpcTestCommand;
import org.mineacademy.orion.command.PacketTestCommand;
import org.mineacademy.orion.command.PermCommand;
import org.mineacademy.orion.command.PreferencesCommand;
import org.mineacademy.orion.command.QuestCommand;
import org.mineacademy.orion.command.RankCommand;
import org.mineacademy.orion.command.RpgCommand;
import org.mineacademy.orion.command.SpawnEntityCommand;
import org.mineacademy.orion.command.StatsCommand;
import org.mineacademy.orion.command.TaskCommand;
import org.mineacademy.orion.event.DatabaseListener;
import org.mineacademy.orion.event.LocaleListener;
import org.mineacademy.orion.event.PlayerListener;
import org.mineacademy.orion.event.ProjectileListener;
import org.mineacademy.orion.hook.DiscordSRVHook;
import org.mineacademy.orion.hook.EffectLibHook;
import org.mineacademy.orion.hook.ProtocolLibHook;
import org.mineacademy.orion.hook.VotifierHook;
import org.mineacademy.orion.mysql.OrionDatabase;
import org.mineacademy.orion.quest.QuestListener;
import org.mineacademy.orion.quest.QuestTask;
import org.mineacademy.orion.rank.RankListener;
import org.mineacademy.orion.rank.RankupTask;
import org.mineacademy.orion.rank.model.Rank;
import org.mineacademy.orion.settings.Settings;
import org.mineacademy.orion.stats.StatsListener;

public class OrionPlugin extends SimplePlugin {

	private EffectLibHook effectLibHook;

	@Override
	protected void onPluginStart() {
		getLogger().info("All works, captain!"); // Use Common#log instead
		System.out.println("All works from system out, boss!"); // Will not display your plugin prefix
		Common.log("Hello from foundation!"); // The recommended way of sending messages

		//LagCatcher.start("mysql");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (final Throwable t) {
			t.printStackTrace();
		}

		//LagCatcher.end("mysql", 0, "Connection to MySQL established. Took {time} ms.");

		registerCommand(new FireworkCommand());
		registerCommand(new SpawnEntityCommand());
		registerCommand(new PermCommand());
		registerCommand(new TaskCommand());
		registerCommand(new PreferencesCommand());
		registerCommand(new RpgCommand());
		registerCommand(new BoardingCommand());
		registerCommand(new BossCommand());
		registerCommand(new CustomEnchantsCommand());
		registerCommand(new StatsCommand());
		registerCommand(new RankCommand());
		registerCommand(new ClassCommand());
		registerCommand(new QuestCommand());
		registerCommand(new CooldownCommand());
		registerCommand(new PacketTestCommand());

		registerEvents(new PlayerListener());
		registerEvents(new ProjectileListener());
		registerEvents(new BossListener());
		registerEvents(new RankListener());
		registerEvents(new ClassListener());
		registerEvents(new QuestListener());
		registerEvents(new StatsListener());
		registerEvents(new DatabaseListener());

		if (HookManager.isDiscordSRVLoaded())
			registerEvents(new DiscordSRVHook());

		if (HookManager.isNuVotifierLoaded())
			registerEvents(new VotifierHook());

		if (Common.doesPluginExist("EffectLib")) {
			effectLibHook = new EffectLibHook();

			registerEvents(effectLibHook);
		}

		Common.setTellPrefix("[Orion]");
		Common.setLogPrefix(Common.getTellPrefix());

		// {orion_rank} (ChatControl) --> the current rank (in colors)
		// %orion_rank% (other plugins) --> ^
		HookManager.addPlaceholder("rank", player -> {
			final PlayerCache cache = PlayerCache.getCache(player);
			final Rank rank = cache.getRank();

			return rank.getColor() + rank.getName();
		});

		for (final ClassBase classBase : ClassBase.getClasses()) {

			// Magic Warrior --> magic_warrior
			HookManager.addPlaceholder("class_" + classBase.getName().toLowerCase().replace(" ", "_"), player -> {
				final PlayerCache cache = PlayerCache.getCache(player);
				final int tier = cache.getClassTier(classBase);

				return tier + "";
			});
		}

		Variables.addVariable("player_deaths", sender -> sender instanceof Player ? PlayerCache.getCache((Player) sender).getDeaths() + "" : "0");
	}

	@Override
	protected void onPluginStop() {
		if (Common.doesPluginExist("EffectLib") && effectLibHook != null)
			effectLibHook.shutdown();
	}

	// This method is called when you start the plugin AND when you reload it
	@Override
	protected void onReloadablesStart() {
		// Those tasks are cancelled on reload so they are only run once each time
		new BroadcasterTask().runTaskTimer(this, 0, Settings.BROADCASTER_DELAY.getTimeTicks());
		new BossTimedTask().runTaskTimer(this, 0, 20 /* this will run every second (20 ticks = 1 second) */);
		new RankupTask().runTaskTimer(this, 0, /*60 **/ 20 /* ONE SECOND --> you want to increase that number for better performance */);
		new QuestTask().runTaskTimer(this, 0, 20);

		// Events here are disabled on reload
		if (Settings.LOG_PLAYERS_LOCALE)
			registerEvents(new LocaleListener());

		// Commands here are overriden on reload so they are registered only once
		if (HookManager.isCitizensLoaded())
			registerCommand(new NpcTestCommand());
		else
			Common.log("Citizens not found, limited functionality available!");

		// Packet listeners are automatically unregistered on reload as well
		if (HookManager.isProtocolLibLoaded())
			ProtocolLibHook.addPacketListeners();

		// Save player data to MySQL
		for (final Player player : Remain.getOnlinePlayers()) {
			final PlayerCache cache = PlayerCache.getCache(player);

			OrionDatabase.getInstance().save(player.getName(), player.getUniqueId(), cache);
		}

		// Save the data.db file
		DataFile.getInstance().save();

		// Clear the cache in the plugin so that we load it fresh for only players that are online right now,
		// saving memory
		PlayerCache.clearAllData();
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();

		// Send the player a simple colored message,
		// for color codes see https://minecraft.gamepedia.com/Formatting_codes
		Common.tellLater(2, player, "&eHello &9from &cFoundation");

		// Add five emeralds to the players inventory
		if (Settings.Join.GIVE_EMERALD)
			player.getInventory().addItem(new ItemStack(Material.EMERALD, 5));

		// Important! The Variables.replace is NOT called automatically when you use "tell" or Replacer.
		// You must call this manually like you see here. This is a feature that saves performance
		// because replacing thousands of variables can put a strain on your server so that you only replace
		// them when you need them.
		final String replacedMessage = Variables.replace("Hello {player} you died {player_deaths} times and have {player_ping}ms ping.", player);

		Common.tell(player, replacedMessage);
	}

	@EventHandler
	public void onEntityDamage(final EntityDamageByEntityEvent event) {
		final Entity victim = event.getEntity();

		// If the damager is a player and the hit entity (victim) is a cow,
		// create explosion with the size of 2 at the cows location
		if (event.getDamager() instanceof Player && victim instanceof Cow && Settings.Entity_Hit.EXPLODE_COW)
			victim.getWorld().createExplosion(victim.getLocation(), Settings.Entity_Hit.POWER.floatValue());
	}

	/*@EventHandler
	public void onBossSpawn(final BossSpawnEvent event) {
		Common.log("Spawning " + event.getBoss().getName() + " at " + Common.shortLocation(event.getLocation()));
	
		//event.setCancelled(true);
	}*/
}
