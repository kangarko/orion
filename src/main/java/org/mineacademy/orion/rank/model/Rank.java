package org.mineacademy.orion.rank.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.orion.PlayerCache;

import lombok.Getter;

/**
 * The core model class for our ranking system. Ranking system has linear progression
 * so players can only upgrade to the next rank (Newbie -> Advanced -> Master)
 */
public abstract class Rank {

	/**
	 * Stores active ranks by their name, so they are singletons
	 *
	 * Ranks are stored by name, this is registered automatically
	 * when you call a static method from this call.
	 *
	 * The reason why we are storing them is the getByName method below
	 */
	private static final Map<String, Rank> byName = new HashMap<>();

	// --------------------------------------------------------------------------------------------------------------
	// Place your ranks below, make sure to give them protected constructor to prevent accidental initializing
	// --------------------------------------------------------------------------------------------------------------

	public static final Rank STUDENT = new RankStudent();
	public static final Rank CRAFTSMAN = new RankCraftsman();
	public static final Rank HERO = new RankHero();
	public static final Rank MASTER = new RankMaster();

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Get the rank name
	 */
	@Getter
	private final String name;

	/**
	 * Get the color of this rank, used for example in chat and rank variables
	 */
	@Getter
	private final ChatColor color;

	/**
	 * Create a new rank and puts it into {@link #byName}
	 *
	 * @param name
	 * @param color
	 */
	protected Rank(final String name, final ChatColor color) {
		this.name = name;
		this.color = color;

		byName.put(name, this);
	}

	/**
	 * Ranks have linear progression, so you can return the next level rank
	 * here or null if this rank is the highest one on your server
	 *
	 * @return
	 */
	public Rank getNext() {
		return null;
	}

	// --------------------------------------------------------------------------------------------------------------
	// Upgrading ranks
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * {@link #upgradeToNextRank(Player, boolean)}
	 *
	 * @param player
	 * @return
	 */
	public boolean upgradeToNextRank(final Player player) {
		return upgradeToNextRank(player, false);
	}

	/**
	 * Return false if we could not upgrade the player to the next rank otherwise returns true
	 * This also sets the next rank automatically in the player cache file (data.db)
	 *
	 * @param player
	 * @param force should we still upgrade his rank even if the canUpgrade method returns false?
	 * @return
	 */
	public boolean upgradeToNextRank(final Player player, final boolean force) {
		final Rank nextRank = getNext();

		if (nextRank == null)
			return false;

		final PlayerCache cache = PlayerCache.getCache(player);

		if (canUpgrade(player, cache, nextRank) || force) {

			// Set his rank in cache and save the data.db file
			cache.setRank(nextRank);

			// Call a protected method you can override to do additional stuff when the player ranks up
			onUpgrade(player, nextRank);

			Common.tellNoPrefix(player, "&8[&c!&8] &6You have upgraded to the " + nextRank.getName() + " rank!");
			return true;
		}

		return false;
	}

	/**
	 * Override this to check if the given player can upgrade to the next rank
	 *
	 * This is only called if the {@link #getNext()} returns a valid rank and you call
	 * {@link #upgradeToNextRank(Player, boolean)} methods
	 *
	 * @param player
	 * @param cache
	 * @param next
	 * @return
	 */
	protected boolean canUpgrade(final Player player, final PlayerCache cache, final Rank next) {
		return false;
	}

	/**
	 * Override this to do additional actions AFTER the rank of the player has been upgraded
	 *
	 * @param player
	 * @param next
	 */
	protected void onUpgrade(final Player player, final Rank next) {
	}

	// --------------------------------------------------------------------------------------------------------------
	// Static access
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Get the first rank everyone gets on join
	 *
	 * @return
	 */
	public static final Rank getFirstRank() {
		return STUDENT;
	}

	/**
	 * Get a rank by name
	 *
	 * @param name
	 * @return
	 */
	public static final Rank getByName(final String name) {
		return byName.get(name);
	}

	/**
	 * Return all ranks
	 *
	 * @return
	 */
	public static final Collection<Rank> getRanks() {
		return Collections.unmodifiableCollection(byName.values());
	}

	/**
	 * Return all ranks as their names
	 *
	 * @return
	 */
	public static final Set<String> getRanksNames() {
		return Collections.unmodifiableSet(byName.keySet());
	}
}
