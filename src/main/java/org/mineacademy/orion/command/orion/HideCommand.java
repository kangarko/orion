package org.mineacademy.orion.command.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.orion.OrionPlugin;

public class HideCommand extends TargettedCommand {

	protected HideCommand(final SimpleCommandGroup parent) {
		super(parent, "hide|h");

		setDescription("Hide the target player from you.");
		//setPermission(null); --> everyone can use it
	}

	@Override
	protected void onCommandFor(final Player target) {
		checkConsole();
		checkBoolean(!target.getName().equals(getPlayer().getName()), "You cannot hide from yourself :-)");

		if (target.canSee(getPlayer())) {
			target.hidePlayer(OrionPlugin.getInstance(), getPlayer());
			tell("&aPlayer " + target.getName() + " can no longer see you.");

		} else {
			target.showPlayer(OrionPlugin.getInstance(), getPlayer());
			tell("&6Player " + target.getName() + " can no longer see you.");
		}
	}
}
