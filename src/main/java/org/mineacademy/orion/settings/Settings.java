package org.mineacademy.orion.settings;

import java.util.List;

import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.settings.SimpleSettings;

// Update: added this suppress warnings annotation for Eclipse users
@SuppressWarnings("unused")
public class Settings extends SimpleSettings {

	@Override
	protected int getConfigVersion() {
		return 1;
	}

	public static class Menu {
		public static String MENU_PREFERENCES_TITLE;
		public static String MENU_EGG_SELECTION_TITLE;

		private static void init() {
			setPathPrefix("Menu");

			MENU_PREFERENCES_TITLE = getString("Preferences_Title");
			MENU_EGG_SELECTION_TITLE = getString("Egg_Selection_Title");
		}
	}

	public static class Join {
		public static Boolean GIVE_EMERALD;

		private static void init() {
			setPathPrefix("Join");

			GIVE_EMERALD = getBoolean("Give_Emerald");
		}
	}

	public static class Entity_Hit {
		public static Boolean EXPLODE_COW;
		public static Double POWER;

		private static void init() {
			setPathPrefix("Entity_Hit");

			EXPLODE_COW = getBoolean("Explode_Cows");
			POWER = getDouble("Power");
		}
	}

	public static List<String> IGNORED_LOG_COMMANDS;
	public static Boolean LOG_PLAYERS_LOCALE;
	public static SimpleTime BROADCASTER_DELAY;

	private static void init() {
		setPathPrefix(null);

		IGNORED_LOG_COMMANDS = getStringList("Ignored_Log_Commands");
		LOG_PLAYERS_LOCALE = getBoolean("Log_Players_Locale");
		BROADCASTER_DELAY = getTime("Broadcaster_Delay");
	}
}
