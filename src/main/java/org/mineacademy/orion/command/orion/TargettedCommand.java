package org.mineacademy.orion.command.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.command.SimpleSubCommand;

public abstract class TargettedCommand extends SimpleSubCommand {

	protected TargettedCommand(final SimpleCommandGroup parent, final String sublabel) {
		super(parent, sublabel);

		setMinArguments(1);
		setUsage("<target>");
	}

	@Override
	protected final void onCommand() {
		final Player target = findPlayer(args[0]);

		onCommandFor(target);
	}

	protected abstract void onCommandFor(Player target);
}
