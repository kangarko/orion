package org.mineacademy.orion.boss.skill;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.orion.boss.SpawnedBoss;

/**
 * Represents the core model for a special boss ability
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class BossSkill {

	/**
	 * The name of the skill
	 */
	private final String name;

	/**
	 * The delay before the boss can execute this skill again
	 */
	@Setter(AccessLevel.PROTECTED)
	private int delaySeconds = 0;

	/**
	 * Last executed time, only used internally to determine whether we can execute this skill,
	 * see {@link #delaySeconds}
	 */
	private long lastExecutedTime = 0;

	// --------------------------------------------------------------------------------------------------------------
	// Running this skill
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Return true if we can run this skill and also update last executed time
	 *
	 * @return
	 */
	public final boolean checkLastRun() {
		if (delaySeconds == 0)
			return true;

		if (lastExecutedTime == 0 || System.currentTimeMillis() - lastExecutedTime > delaySeconds * 1000) {
			updateLastExecutedTime();

			return true;
		}

		return false;
	}

	private void updateLastExecutedTime() {
		lastExecutedTime = System.currentTimeMillis();
	}

	// --------------------------------------------------------------------------------------------------------------
	// Events called automatically if the boss is having this skill
	// See BossListener
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Called automatically when the boss having this skill attacks something
	 *
	 * @param spawnedBoss
	 * @param event
	 */
	public void onBossAttack(final SpawnedBoss spawnedBoss, final EntityDamageByEntityEvent event) {
	}

	/**
	 * Called automatically when something attacks the boss having this skill
	 *
	 * @param spawnedBoss
	 * @param event
	 */
	public void onBossDamaged(final SpawnedBoss spawnedBoss, final EntityDamageByEntityEvent event) {
	}

	/**
	 * Return true if the given object is a boss skill with the same name
	 *
	 * @param obj
	 * @return
	 */
	@Override
	public final boolean equals(final Object obj) {
		return obj instanceof BossSkill && ((BossSkill)obj).getName().equals(this.name);
	}
}
