package org.mineacademy.orion.hook;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.orion.mysql.OrionDatabase;
import org.mineacademy.orion.vote.PlayerVote;
import org.mineacademy.orion.vote.VoteRewards;

import java.util.List;

/**
 * Supports nuVotifier and rewards players for voting, even offline
 */
public class VotifierHook implements Listener {

	/**
	 * Store the database field for convenient access
	 */
	private final OrionDatabase voteDatabase = OrionDatabase.getInstance();

	/**
	 * Checks if the player has voted while offline and rewards them
	 *
	 * @param event
	 */
	// If you want to give remaining vote rewards even when nuVotifier
	// is not installed, move this event to a separate PlayerListener class
	@EventHandler
	public final void onJoin(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();

		Common.runLaterAsync(() -> {
			if (voteDatabase.hasPendingVote(player.getName())) {
				voteDatabase.removePendingVotes(player);

				final List<PlayerVote> votes = voteDatabase.loadVotes(player);

				// Reward the player after 20 ticks (1 second)
				Common.runLater(20, () -> VoteRewards.onVote(player, votes));
			}
		});

		// NB: If you have AuthMe Reloaded installed,
		// you want to listen to the following events
		// to only give votes when the player has logged in or
		// registered for the first time.
		//
		// PS: Ensure AuthMe is installed using HookManager.isAuthMeLoaded
		//
		// fr.xephi.authme.events.LoginEvent;
		// fr.xephi.authme.events.RegisterEvent;
	}

	/**
	 * Processes an incoming vote. Rewards the player
	 * if online, otherwise stores into MySQL as a pending vote.
	 *
	 * Either way, the vote is saved into MySQL into the votes table.
	 *
	 * @param event
	 */
	@EventHandler
	public final void onVote(final VotifierEvent event) {
		final Vote vote = event.getVote();
		final String playerName = vote.getUsername();
		final Player player = Bukkit.getPlayer(playerName);

		final PlayerVote wrappedVote = new PlayerVote(playerName, vote.getServiceName(), System.currentTimeMillis());

		Common.runLaterAsync(() -> {
			// Save the vote
			voteDatabase.saveVote(wrappedVote);

			// Reward player instantly if online
			if (player != null && player.isOnline()) {
				final List<PlayerVote> votes = voteDatabase.loadVotes(player);

				Common.runLater(() -> VoteRewards.onVote(player, votes));
			}

			// Store a pending vote into the database
			else
				voteDatabase.addPendingVote(wrappedVote);
		});
	}
}