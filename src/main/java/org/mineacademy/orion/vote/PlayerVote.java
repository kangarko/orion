package org.mineacademy.orion.vote;

import lombok.Data;

/**
 * Represents a simple player vote
 */
@Data
public final class PlayerVote {

	/**
	 * The player name
	 */
	private final String playerName;

	/**
	 * The website he used to vote from
	 */
	private final String serviceName;

	/**
	 * The timestamp in milliseconds of when
	 * we registered the vote
	 */
	private final long timestamp;
}