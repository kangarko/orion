package org.mineacademy.orion.boss;

import org.mineacademy.fo.menu.model.ItemCreator;

import lombok.Data;

@Data // --> constructor, getter and even setters if fields are not final
public class BossEquipment {

	private final ItemCreator helmet, chestplate, leggings, boots;
}
