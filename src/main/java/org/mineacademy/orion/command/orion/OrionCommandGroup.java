package org.mineacademy.orion.command.orion;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommandGroup;

@AutoRegister
public final class OrionCommandGroup extends SimpleCommandGroup {

	@Override
	protected void registerSubcommands() {
		registerSubcommand(new StrikeCommand(this));
		registerSubcommand(new HideCommand(this));
		registerSubcommand(new FireCommand(this));
		registerSubcommand(new ReloadCommand()); // Uses the OrionPlugin#getMainCommand as the parent, which is this
	}

	@Override
	protected String getCredits() {
		return "Visit yourname.com for more information.";
	}
}
