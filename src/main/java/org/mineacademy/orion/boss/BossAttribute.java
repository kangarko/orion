package org.mineacademy.orion.boss;

import lombok.Data;
import org.mineacademy.fo.remain.CompAttribute;

@Data
public class BossAttribute {

	private final CompAttribute attribute;
	private final double value;
}
