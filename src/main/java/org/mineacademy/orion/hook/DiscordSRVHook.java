package org.mineacademy.orion.hook;

import java.util.Arrays;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.model.DiscordListener;
import org.mineacademy.fo.remain.Remain;

import github.scarsz.discordsrv.api.events.DiscordGuildMessagePreProcessEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Message;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;

public class DiscordSRVHook extends DiscordListener {

	@Override
	protected void onMessageReceived(final DiscordGuildMessagePreProcessEvent event) {
		final TextChannel channel = event.getChannel();
		final Member member = event.getMember();
		final Message message = event.getMessage();
		final String chatMessage = message.getContentDisplay();

		// Prevent DiscordSRV from outputting the message in the chat, we handle it in a custom way at the bottom
		event.setCancelled(true);

		//Common.log("Is called from the main thread (from heartbeat) ? " + Bukkit.isPrimaryThread());

		// --------------------------------------------------------------------------------------------------------
		// Example 1 - Filtering profanity
		// --------------------------------------------------------------------------------------------------------

		final List<String> profanities = Arrays.asList("fuck", "shit", "cock");

		for (final String profanity : profanities)
			if (chatMessage.contains(profanity)) {
				/*message.delete().complete();
				final Message warningMessage = channel.sendMessage("Please do not use profanity in this channel.").complete();
				warningMessage.delete().completeAfter(2, TimeUnit.SECONDS);*/

				removeAndWarn("Please do not use profanity in this channel.");
			}

		// --------------------------------------------------------------------------------------------------------
		// Example 2 - Running commands from Discord on Bukkit on the main thread
		// --------------------------------------------------------------------------------------------------------

		if (chatMessage.equals("/creative")) {
			final String permission = "orion.discord.command.creative";
			final Player player = findPlayer(member.getEffectiveName(), "You are not connected to the server (" + member.getEffectiveName() + ")!");

			checkBoolean(PlayerUtil.hasPerm(player, permission), "Insufficient permission (" + permission + ")!");

			Common.runLater(() -> player.setGameMode(GameMode.CREATIVE));

			removeAndWarn("Your gamemode has been changed to creative.");
		}

		// --------------------------------------------------------------------------------------------------------
		// Example 3 - Clearing channel message history on Discord
		// --------------------------------------------------------------------------------------------------------

		if (chatMessage.equals("/clear")) {
			checkBoolean(hasRole(member, "admin"), "Only members with the 'admin' role can clear channel messages.");

			for (final Message oldMessage : channel.getIterableHistory().complete()) {
				final long distanceFromNow = (System.currentTimeMillis() / 1000) - oldMessage.getTimeCreated().toEpochSecond();

				if (distanceFromNow > 3600 * 24 * 30) // Only delete messages older than 30 days
					oldMessage.delete().complete();
			}

			removeAndWarn("Chat channel has been cleared.");
		}

		// --------------------------------------------------------------------------------------------------------
		// Example 4 - Checking for role and preventing chatting if the role is lacking
		// --------------------------------------------------------------------------------------------------------

		if (channel.getName().equals("vip") && !hasRole(member, "VIP"))
			removeAndWarn("Only VIP players can post to this channel!");

		if (channel.getName().equals("minecraft"))
			for (final Player player : Remain.getOnlinePlayers())
				Common.tellNoPrefix(player, "&8[&bDiscord&8] &7" + member.getEffectiveName() + ": &f" + chatMessage);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onMessageSend(final AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final String message = event.getMessage();

		sendMessage(player, "minecraft", Common.stripColors(message)); // Adds player name before automatically
		//sendMessage("minecraft", Common.stripColors(message));
	}
}
