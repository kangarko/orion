package org.mineacademy.orion.conversation;

import javax.annotation.Nullable;

import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.conversation.SimpleConversation;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.fo.remain.CompSound;

import lombok.NonNull;

public class ExpPrompt extends SimplePrompt {

	public ExpPrompt() {
		super(false);
	}

	@Override
	protected String getPrompt(final ConversationContext ctx) {
		return "&6Write the amount of exp levels you want to receive.";
	}

	@Override
	protected boolean isInputValid(final ConversationContext context, final String input) {
		if (!Valid.isInteger(input))
			return false;

		final int level = Integer.parseInt(input);

		return level > 0 && level < 9009;
	}

	@Override
	protected String getFailedValidationText(final ConversationContext context, final String invalidInput) {
		return "Only specify a non-zero number.";
	}

	@Override
	protected @Nullable Prompt acceptValidatedInput(@NonNull final ConversationContext context, @NonNull final String input) {
		final int level = Integer.parseInt(input);
		final Player player = getPlayer(context);

		player.setLevel(level);
		CompSound.LEVEL_UP.play(player);

		tell(context, "&6You now have " + level + " experience levels.");
		return Prompt.END_OF_CONVERSATION;
	}

	@Override
	public void onConversationEnd(final SimpleConversation conversation, final ConversationAbandonedEvent event) {
		// Do more stuff when the conversation is over
	}
}
