package org.mineacademy.orion.quest.model;

import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.orion.classes.model.ClassBase;

public class QuestTripleVote extends Quest {

	protected QuestTripleVote() {
		super("Triple Vote");
	}

	@Override
	public Quest getNext(final ClassBase classBase) {
		return null;
	}

	@Override
	public CompMaterial getIcon() {
		return CompMaterial.TOTEM_OF_UNDYING;
	}

	@Override
	public String[] getMenuLore() {
		return new String[] {
				"Vote for our server",
				"at least three times."
		};
	}

	@Override
	public String getCompletion(final Player player, final Object completionData) {
		return null;
	}
}
