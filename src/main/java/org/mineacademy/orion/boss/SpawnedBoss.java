package org.mineacademy.orion.boss;

import lombok.Data;
import org.bukkit.entity.LivingEntity;
import org.mineacademy.orion.boss.model.Boss;

@Data
public final class SpawnedBoss {

	private final Boss boss;
	private final LivingEntity entity;
}
