package org.mineacademy.orion.tool;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.orion.util.OrionUtil;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class KittyBow extends Tool implements Listener {

	@Getter
	private final static Tool instance = new KittyBow();

	private final Map<Location, Vector> explodedLocations = new HashMap<>();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.BOW,
				"&bKitty Bow",
				"",
				"Right click air to launch",
				"explosive arrows...",
				"PS: They'll damage blocks!")
				.glow(true)
				.make();
	}

	@EventHandler
	public void onProjectileLaunch(final ProjectileLaunchEvent event) {
		if (!(event.getEntity().getShooter() instanceof Player))
			return;

		final Projectile projectile = event.getEntity();
		final Player player = (Player) projectile.getShooter();

		// Checks if the players item in their hand is similar to this item
		if (!ItemUtil.isSimilar(player.getItemInHand(), getItem()))
			return;

		if (!OrionUtil.checkKittyArrow(player, event)) {
			//event.setCancelled(true); // Update: Actually unnecessary since we cancel that in the method checkKittyArrow

			return;
		}

		// Mark the shot arrow so that we can catch it later
		CompMetadata.setMetadata(projectile, "KittyArrow"); // Persistent tags for any entity. NB: Minecraft 1.8.x loses them on restart/reload
	}

	@EventHandler
	public void onProjectileHit(final ProjectileHitEvent event) {

		// Catch the shot arrow so that we can make an explosion at the hit location
		if (!CompMetadata.hasMetadata(event.getEntity(), "KittyArrow"))
			return;

		final Projectile projectile = event.getEntity();

		// Before the explosion we must store where the block that this explosion will come from is located
		// as well as the direction and speed of the arrow
		explodedLocations.put(projectile.getLocation().getBlock().getLocation(), projectile.getVelocity());

		projectile.remove();
		projectile.getWorld().createExplosion(projectile.getLocation(), 4F);
	}

	@EventHandler
	public void onBlockExplode(final BlockExplodeEvent event) {

		// We try to remove the exploded block location we stored above
		// This method is failsafe and simply returns null if nothing was removed
		final Vector vector = explodedLocations.remove(event.getBlock().getLocation());

		if (vector != null) {
			for (final Block block : event.blockList())
				// Use "chance" for chances between 0-100 or "chanceD" for whole numbers (chances between 0.00 for 0% and 1.00 for 100%)
				if (RandomUtil.chanceD(0.45)) // Chances are 45%

					// Shot the given block with the vector stored in the map when the arrow hit the ground above
					BlockUtil.shootBlock(block, vector);
				else

					// Otherwise just remove the block
					block.setType(CompMaterial.AIR.getMaterial());

			// Prevent or greatly reduce the amount of dropped items
			event.setYield(0F);
		}
	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {
		// We do not use this since the Bow must be charged to launch arrows and simply clicking wont do anything
	}
}
