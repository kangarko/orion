package org.mineacademy.orion.command;

import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;

public class FireworkCommand extends SimpleCommand {

	public FireworkCommand() {
		super("firework"); // The firework is the command label to run this command, here /firework in the game
	}

	@Override
	protected void onCommand() {
		// Available fields:
		// sender -> the CommandSender, which can be a Player or a ConsoleCommandSender, use "instanceof" to find out or use checkConsole()
		// args -> the string array of command arguments, /tell kangarko hello there has the args[0] to "hello" and args[1] to "there"

		// Automatically prevent the console from running this command
		// I'll show you how to customize the no-console message in a later week
		checkConsole();

		final Player player = getPlayer(); // getPlayer will try to convert the sender into the player

		// Spawn a firework at the players location
		player.getWorld().spawn(player.getLocation(), Firework.class);

		final String oldPrefix = Common.getTellPrefix();
		Common.setTellPrefix("&8[&dFirework&8] &7");

		// Tell the sender of this command a message directly
		tell("&6A firework has been spawned!");

		Common.setTellPrefix(oldPrefix);

		Common.log("Hello from firework");
	}
}
