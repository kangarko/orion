package org.mineacademy.orion.vote;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.orion.PlayerCache;
import org.mineacademy.orion.PlayerCache.ClassCache;
import org.mineacademy.orion.quest.model.Quest;

import java.util.List;

/**
 * A very primitive model of rewarding players
 */
public final class VoteRewards {

	/**
	 * Rewards the player based on how many votes he did.
	 *
	 * We check for the vote count from MySQL in the method.
	 *
	 * @param player
	 * @param votes
	 */
	public static void onVote(final Player player, final List<PlayerVote> votes) {
		Valid.checkSync("Rewarding players must happen on the main thread!");

		// His first vote
		if (votes.size() == 1) {
			Common.tell(player, "&6Thank you for your first vote! Here's a quick reward.");

			CompMaterial.IRON_HORSE_ARMOR.give(player);
		}

		// Reached 3 or more votes
		else if (votes.size() == 3) {
			final PlayerCache cache = PlayerCache.getCache(player);

			// Indicates if we completed the triple vote quest, for any class
			boolean completed = false;

			for (final ClassCache classCache : cache.getClasses())
				if (!cache.hasCompletedQuest(Quest.TRIPLE_VOTE, classCache.getClassBase())) {
					completed = true;

					cache.setCompletedQuest(Quest.TRIPLE_VOTE, classCache);
				}

			Common.tell(player, completed ? "&6You voted 3x and have now completed the Triple Vote quest." : "&6You voted 3x but already completed Triple Vote quest.");
		}

		else
			Common.tell(player, "&6Thank you for your vote! You voted " + Common.plural(votes.size(), "time") + " so far.");

		CompSound.FIREWORK_TWINKLE2.play(player);
	}
}
