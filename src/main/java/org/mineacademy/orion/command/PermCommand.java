package org.mineacademy.orion.command;

import org.bukkit.permissions.PermissionAttachment;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.orion.OrionPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermCommand extends SimpleCommand {

	private final static String DEMO_PERMISSION = "my.simple.permission";

	// TODO Highest coding standards say to only have command execution code in the command class,
	// so you are recommended to move this into a separate PermissionManager class of your plugin
	private final Map<UUID, PermissionAttachment> permissions = new HashMap<>();

	public PermCommand() {
		super("perm");

		setPermission(null);
		setMinArguments(1);
		setUsage("<add|remove>");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0].toLowerCase();

		// Adds a new permission attachment to sender
		if ("add".equals(param)) {
			checkBoolean(!permissions.containsKey(getPlayer().getUniqueId()), "Run /{label} remove to remove the demo permission first.");

			tell("Do you have the demo permission before? " + hasPerm(DEMO_PERMISSION));

			// Call Bukkit's method to add a new permission attachment to the sender with the value of 'true'
			final PermissionAttachment perm = getPlayer().addAttachment(OrionPlugin.getInstance(), DEMO_PERMISSION, true);
			tell("Do you have the demo permission after? " + hasPerm(DEMO_PERMISSION));

			// Store our permission in the map along with the player unique ID (we do not use names since players can change their name in Minecraft)
			permissions.put(getPlayer().getUniqueId(), perm);

		} else if ("remove".equals(param)) {

			final PermissionAttachment perm = permissions.remove(getPlayer().getUniqueId());
			checkNotNull(perm, "Run /{label} add first to give yourself the demo permission.");

			tell("Do you have the demo permission before? " + hasPerm(DEMO_PERMISSION));

			// Prevent Bukkit throwing errors if the sender has left in the meanwhile and his permission attachment got removed
			if (hasPerm(DEMO_PERMISSION))
				// Remove the permission attachment from sender using the one we store in our map above since we cannot simply
				// type the permission because multiple plugins can have the same permission names
				getPlayer().removeAttachment(perm);

			tell("Do you have the demo permission after? " + hasPerm(DEMO_PERMISSION));

		} else
			returnInvalidArgs();
	}
}
