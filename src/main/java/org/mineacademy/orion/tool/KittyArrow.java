package org.mineacademy.orion.tool;

import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class KittyArrow extends Tool {

	@Getter
	private final static Tool instance = new KittyArrow();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.SPECTRAL_ARROW,
				"&6Kitty Arrow",
				"",
				"Use this as ammunition",
				"in KittyBow and KittyCannon",
				"tools!")
				.glow(true)
				.make();
	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {
		event.setCancelled(true);
	}
}
