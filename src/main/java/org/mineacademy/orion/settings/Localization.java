package org.mineacademy.orion.settings;

import org.mineacademy.fo.settings.SimpleLocalization;

@SuppressWarnings("unused")
public class Localization extends SimpleLocalization {

	@Override
	protected int getConfigVersion() {
		return 1;
	}

	public static class Command {

		public static class Boss {
			public static String NOT_FOUND;

			private static void init() {
				setPathPrefix("Command.Boss");

				NOT_FOUND = getString("Not_Found");
			}
		}
	}

	public static class Boarding {
		public static String CANCELLED;

		private static void init() {
			setPathPrefix("Boarding");

			CANCELLED = getString("Cancelled");
		}
	}

	private static void init() {
		setPathPrefix(null);
	}
}
