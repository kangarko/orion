package org.mineacademy.orion.command.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class FireCommand extends TargettedCommand {

	protected FireCommand(final SimpleCommandGroup parent) {
		super(parent, "fire|f");

		setPermission("orion.fire");
		setDescription("Set the target on fire.");
	}

	@Override
	protected void onCommandFor(final Player target) {
		checkBoolean(!PlayerUtil.hasPerm(target, "dont.put.me.on.fire"), "You cannot put " + target.getName() + "on fire!");

		target.setFireTicks(20 * 4);
		tell("&cSet " + target.getName() + " on fire for four seconds.");
	}
}
